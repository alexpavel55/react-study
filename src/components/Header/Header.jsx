import s from './Header.module.css';
const Header = () => {
    return <header className={s.header}>
        <img src="https://upload.wikimedia.org/wikipedia/commons/5/53/Wikimedia-logo.png" alt=""/>
    </header>
}
export default Header;