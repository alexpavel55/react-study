import s from './MyPosts.module.css';
import React from 'react';
import Posts from "./Post/Posts";
const MyPosts =(props) =>{
    // let postData = [
    //     {id:1,message:'Hi, how are you?',likesCount:12},
    //     {id:2,message:'It\'s my first post',likesCount:1}
    // ]
    let postElements = props.posts.map(p => <Posts message={p.message} likesCount={p.likesCount} />);
    let newPostElement = React.createRef();
    let addPost = () => {
        let text = newPostElement.current.value;
        props.addPost(text);
    }
    return  <div>
        <h3>My posts</h3>
        <div>
            new post
            <div>
                <textarea ref={newPostElement} name="textarea" id="new-post" cols="30" rows="10"></textarea>
            </div>
            <div>
                <button onClick={addPost}>Add post</button>
            </div>
        </div>
        <div className={s.posts}>
            {postElements}
        </div>
    </div>
}
export default MyPosts;