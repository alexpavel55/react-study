import s from './Navbar.module.css';
import {
    Link
} from 'react-router-dom';
const Navbar =() => {
    return <nav className={s.nav}>
        <div>
            <Link to="/profile">Portfolio</Link>
        </div>
        <div>
            <Link to="/dialogs">Messages</Link>
        </div>
        <div>
            <a href="src/components/Navbar/Navbar">News</a>
        </div>
        <div>
            <a href="src/components/Navbar/Navbar">Music</a>
        </div>
        <div>
            <a href="src/components/Navbar/Navbar">Settings</a>
        </div>
    </nav>
}
export default Navbar;