import './App.css';
import Header from './components/Header/Header.jsx'
import Navbar from './components/Navbar/Navbar.jsx'
import Profile from './components/Profile/Profile.jsx'
import Dialogs from "./components/Dialogs/Dialogs.jsx";
import {BrowserRouter, Route, Routes} from "react-router-dom";

const App = (props) => {
    return (
        <BrowserRouter>
        <div className="app-wrapper">
            <Header/>
            <Navbar/>
            <div className='content'>
                <Routes>
                    <Route path="/profile" element={<Profile posts={props.appState.profilePage.postData}
                                                             addPost={props.addPost}/>} />
                    <Route path="/dialogs" element={<Dialogs dialogs={props.appState.dialogPage.dialogsData}
                                                             messages={props.appState.dialogPage.messagesData}/>} />
                </Routes>
            </div>
        </div>
        </BrowserRouter>);
}

export default App;
