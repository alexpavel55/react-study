import {rerenderEntireTree} from "../render.js"
let state ={
    profilePage : {
        postData : [
        {id:1,message:'Hi, how are you?',likesCount:12},
        {id:2,message:'It\'s my first post',likesCount:1}
    ]
    },
    dialogPage : {
        dialogsData : [
            {name:'Pavel', id: "1"},
            {name:'Yullia', id: "2"},
            {name:'Denys', id: "3"},
            {name:'Nastya', id: "4"},
            {name:'Andrei', id: "5"},
            {name:'Katya', id: "6"},
            {name:'Petr', id: "7"}
        ],
        messagesData : [
            {message:'Pavel', id: "1"},
            {message:'Yullia', id: "2"},
        ]
    }
}
export let addPost = (postMessage) => {
    let newPost = {
        id: 5,
        message: postMessage,
        likesCount:3
    };
    state.profilePage.postData.push(newPost);
    rerenderEntireTree(state);
}
export default state;